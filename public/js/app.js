jQuery(document).ready(function($) {
	jQuery('.carousel').carousel()

	jQuery('#navIcon').click(function(event) {
		/* Act on the event */
		jQuery('#navMenu').removeClass('nav-hide');
		jQuery('#navMenu').addClass('nav-active');
	});


	jQuery('#closeNav').click(function() {
		jQuery('#navMenu').removeClass('nav-active');
		jQuery('#navMenu').addClass('nav-hide');
	});

	
});