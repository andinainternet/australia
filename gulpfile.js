
/* ======================
 * PROJECT NAME
 * ======================
*/
let project_name = 'gfny-theme';

/* ======================
 * PROJECT CONFIG 
 * ======================
*/

	//**Source Project
let project_root 	= './projects/',
	project_slug 	= project_name + '/',
	// src 			= project_root + project_slug,
	src 			= './src/',
	
	//**Theme Child
	wp_theme_name	= project_name + '/',
	
	//**Destiny Files
	// destiny 		= '../wp-content/themes/',
	destiny 		= 'public',
	
	wp_parent_dest	= destiny,
	// wp_parent_theme	= wp_parent_dest + wp_theme_name;
	wp_parent_theme	= wp_parent_dest;

/* ================
 * GULP PLUGINS
 * ================
*/
let autoprefixer	= require('gulp-autoprefixer'),
	babel 			= require('babelify'),
	browserify 		= require('browserify'),
	concatCss 		= require('gulp-concat-css'),
	concatJs 		= require('gulp-concat'),
	data 			= require('gulp-data'),
	gulp 			= require('gulp'),
	pug 			= require('gulp-pug'),
	// jade 			= require('gulp-jade-php'),
	nib 			= require('nib'),
	notify 			= require('gulp-notify'),
	rename 			= require('gulp-rename'),
	stylus 			= require('gulp-stylus'),
	sourcemaps 		= require('gulp-sourcemaps'),
	source 			= require('vinyl-source-stream'),
	plumber 		= require('gulp-plumber'),
	uglify 			= require('gulp-uglify'),
	watchify 		= require('watchify');


/* ================
 * GULP TASKS
 * ================
*/

// Handler Error
let onError = function(err){
	console.log("Se ha producido un error: ", err.message);
	this.emit("end");
}


// Get one .styl file and render 
gulp.task('styl', function () {
  return gulp.src( src + 'css/styl.styl')
	.pipe(plumber({
		errorHandler:onError
	}))
	.pipe(stylus({
		compress: false
	}))
	.pipe(autoprefixer({
            browsers: ['last 99 versions'],
            cascade: false
    }))
	.pipe(rename('style.css'))
	.pipe(gulp.dest( wp_parent_theme))
	/*Include Source Map*/
    .pipe(sourcemaps.init())
    .pipe(stylus())
    .pipe(sourcemaps.write())
    .pipe(rename('style.map'))
    .pipe(gulp.dest( wp_parent_theme));
});

//console.log(wp_parent_theme);

//=========================================================
//START CUSTOM TAKS JUST FOR CLIENTES-ANDINA-DIGITAL
//=========================================================


// // Get one .styl file and render 
// gulp.task('login_styl', function () {
//   return gulp.src( src + 'css/login.styl')
// 	.pipe(plumber({
// 		errorHandler:onError
// 	}))
// 	.pipe(stylus({
// 		compress: false
// 	}))
// 	.pipe(autoprefixer("last 3 versions"))
// 	.pipe(rename('css/login/style.css'))
// 	.pipe(gulp.dest( wp_parent_theme));
// });

// console.log(wp_parent_theme);


// gulp.task('user_support', function () {
//   return gulp.src( src + 'css/support/styl.styl')
// 	.pipe(plumber({
// 		errorHandler:onError
// 	}))
// 	.pipe(stylus({
// 		compress: false
// 	}))
// 	.pipe(autoprefixer("last 3 versions"))
// 	.pipe(rename('support.css'))
// 	.pipe(gulp.dest( wp_parent_theme + '/css/support/'));
// });

// console.log(wp_parent_theme);	



//=========================================================
//ENDS CUSTOM TAKS JUST FOR CLIENTES-ANDINA-DIGITAL
//=========================================================


//Compile Pug Files
gulp.task('templates', function() {
gulp.src( src + 'views/*.pug')
	.pipe(plumber({
		errorHandler:onError
	}))	
	.pipe(pug({
		locals: {
			title: 'Australia' 
		},
		pretty: true
	}))
	.pipe(rename({
			// extname: '.php'
			extname: '.html'
	}))
	.pipe(gulp.dest( wp_parent_theme ));
});


/**Destiny PHP Files**/
// gulp.task('php', function() {
// gulp.src( src + 'php/**/*.php')
// 	.pipe(gulp.dest( wp_parent_theme ));
// });


/**Send all lib to production**/
gulp.task('vendor', function(){
	gulp.src( src + 'vendor/**/*')
 		.pipe(gulp.dest( wp_parent_theme + '/lib/'));
});

//Send Other JS Files to production
gulp.task('copyjs', function(){
	gulp.src( src + 'js/**/*')
		.pipe(gulp.dest( wp_parent_theme + '/js'));
});


//Send all General Files to production
// gulp.task('general', function(){
// 	gulp.src( src + 'general/**/*')
// 		.pipe(gulp.dest( wp_parent_theme));
// });


//Send all assets to public
gulp.task('assets', function(){
	gulp.src( src + 'assets/**')
		.pipe(gulp.dest( wp_parent_theme + '/assets'));
});



// ACTIVAR SOLO CON EDGARDO
// gulp.task('concat-js', function() 
// {
//   gulp.src(src + 'js/modules/**/*.js')
// 	.pipe(concatJs('concat.js'))
// 	// .pipe(uglify())
// 	.pipe(gulp.dest( src + 'js/'))
// 	// .pipe(notify("Ha finalizado la Concat js!"));
// });



//Tasks Javascript
// gulp.task('bundle', function() {
// var bundle = browserify( src + 'js/index.js', {debug: true});
	
// 		bundle
// 			.transform(babel, { 
// 							presets: [ 'es2015' ], 
// 							plugins: [ 'syntax-async-functions', 
// 										'transform-regenerator' 
// 									]})
// 			.bundle()
// 			.on('error', function(err) { console.log(err); this.emit('end') })
// 			.pipe(source(src + 'js/index.js'))
// 			.pipe(rename('app.js'))
// 			.pipe(gulp.dest( wp_parent_theme + 'js/'));
	
// });



//Function for watching changue on real time in console
gulp.task('watch', function (){
	gulp.watch( src + 'css/**/*.styl', [stylus]);
	gulp.watch( src + 'views/**/*.pug', [pug]);
	gulp.watch( src + 'vendor/**/*', []);
	gulp.watch( src + 'js/**/*', []);
	// gulp.watch( src + 'php/**/*.php', []);
	// gulp.watch( src + 'general/**/*', []);
	// gulp.watch( src + 'js/index.js', [browserify]);
});



//RUN TASKS
gulp.task('default', ['styl', 'vendor', 'copyjs', 'assets', 'templates', 'watch']);
